const routes = require('express').Router()
const task = require('../controllers/taskController')

routes.get('/tasks', task.list)

routes.get('/task/:id', task.listDetail)

routes.post('/task', task.new)

routes.patch('/task/:id', task.update)

routes.delete('/task/:id', task.delete)

module.exports = routes
