const router = require('express').Router()
const taskRouter = require('./taskRouter')

router.use(taskRouter)

module.exports = router