##install

project ini menggunakan framework expressJS dan orm dari sequelize.

untuk melakukan instalasi
1. npm install
2. sequelize init
3. cek pada file config.json didalam folder json
4. lakukan konfigurasi sesuai dengan akun mysql
5. sequelize db:create
5.1 jika terjadi error ketikan node_modules/.bin/sequelize db:create
6. sequelize db:migrate
6.1 jika terjadi error ketikan node_modules/.bin/sequelize db:migrate
7. Npm run dev
8. sistem akan berjalan pada port 2006