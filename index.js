const { response } = require('express')
const express = require('express')
const app = express()
const port = process.env.PORT || 2006
const router = require('./routes')

app.listen(port, () =>{
    console.log(`server running at port ${port}`);
})

app.use(express.json())

app.use(router)