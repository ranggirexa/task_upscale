const { Task } = require("../models");

module.exports = {
  list: async (req, res) => {
    const data = await Task.findAll({
    });
    if (data.length == 0) {
      return res.status(404).json({
        status: 404,
        message: "data tidak ditemukan",
      });
    }
    res.status(200).json({
      status: 200,
      message: "successfull",
      response: data,
    });
  },

  listDetail: async (req, res) => {
    const data = await Task.findAll({
      where: {
        id: req.params.id,
      },
    });
    if (data.length == 0) {
      return res.status(404).json({
        status: 404,
        message: "data tidak ditemukan",
      });
    }
    res.status(200).json({
      status: 200,
      message: "successfull",
      response: data,
    });
  },

  new: async (req, res) => {
    const { title = "", description = "" } = req.body;
    if (title == "") {
      return res.status(400).json({
        status: 400,
        message: "title tidak boleh kosong",
      });
    } else if (description == "") {
      return res.status(400).json({
        status: 400,
        message: "description tidak boleh kosong",
      });
    } else {
      const create = await Task.create({
        title: title,
        description: description,
        completed: req.body.completed,
      });
      if (create == null) {
        return res.status(500).json({
          status: 500,
          message: "server error",
        });
      }
      res.status(200).json({
        status: 200,
        message: "successfull insert new data",
        response: create,
      });
    }
  },

  update: async (req, res) => {
    const { id } = req.params;

    const update = await Task.update(
      {
        title: req.body.title,
        description: req.body.description,
		completed:req.body.completed
      },
      {
        where: {
          id: id,
        },
      }
    );
    if (update == null) {
      return res.status(500).json({
        status: 500,
        message: "server error",
      });
    }
    res.status(200).json({
      status: 200,
      message: "successfull update new data",
      response: update,
    });
  },

  delete: async (req, res) => {
    const { id } = req.params;
    const del = await Task.destroy({
      where: {
        id: id,
      },
    });
    if (del == null) {
      return res.status(500).json({
        status: 500,
        message: "server error",
      });
    }
    res.status(200).json({
      status: 200,
      message: "successfull delete new data",
      response: del,
    });
  },
};
